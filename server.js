

// init project
var express = require('express');
var app = express();

// enable CORS (https://en.wikipedia.org/wiki/Cross-origin_resource_sharing)
// so that your API is remotely testable by FCC 
var cors = require('cors');
app.use(cors({optionSuccessStatus: 200}));  // some legacy browsers choke on 204

app.use(express.static('public'));

app.get("/", function (req, res) {
  res.sendFile(__dirname + '/views/index.html');
});

function isValidDate(date) {
  return date instanceof Date && !isNaN(date);
}
app.get("/api/timestamp/:date_string?", function (req, res) {
  const dateString = req.params.date_string;
  if (/\d{5,}/.test(dateString)) {
    const dateInt = parseInt(dateString);
    if (!Number.isNaN(dateInt)) {
      return res.json({ unix: dateInt, utc: new Date(dateInt*1000).toUTCString() });
    }
  }
  const date = (dateString ? new Date(dateString): new Date());
  if (isValidDate(date)) {
    return res.json({ unix: date.getTime(), utc: date.toUTCString() });
  }
  res.json({ unix: null, utc: 'Invalid Date', error: 'Invalid Date' });
});



// listen for requests :)
var listener = app.listen(process.env.PORT, function () {
  console.log('Your app is listening on port ' + listener.address().port);
});
